#include "Terminal.h"

#if defined(_WIN32)
#include <curses.h>
#else
#include <ncurses.h>
#endif


Terminal::Terminal(int width, int height)
{
	initscr();
	resize_term(height, width);

	cbreak();
	keypad(stdscr, true);
	noecho();
	curs_set(0);
	timeout(0);
	start_color();
}

int Terminal::width() const
{
	return COLS;
}

int Terminal::height() const
{
	return LINES;
}

void Terminal::set_cell(int x, int y, char symbol, int pairNumber)
{
	switch(pairNumber) {
		case 1: init_pair(1, COLOR_MAGENTA, COLOR_BLACK);
		case 2: init_pair(2, COLOR_GREEN, COLOR_BLACK);
		case 3: init_pair(3, COLOR_RED, COLOR_BLACK);
		case 4: init_pair(4, COLOR_YELLOW, COLOR_BLACK);
		case 5: init_pair(5, COLOR_CYAN, COLOR_BLACK);
		case 6: init_pair(6, COLOR_BLUE, COLOR_BLACK);
		case 7: init_pair(7, COLOR_WHITE, COLOR_BLACK);
		case 8: init_pair(8, COLOR_RED, COLOR_YELLOW);
		case 9: init_pair(9, COLOR_YELLOW, COLOR_RED);
	}
    attrset(COLOR_PAIR(pairNumber));
	mvaddch(y, x, symbol);
	attroff(COLOR_PAIR(pairNumber));
}

void Terminal::draw_text(int x, int y, const std::string& text)
{
	mvaddstr(y, x, text.c_str());
}

void Terminal::clear()
{
	::clear();
}

void Terminal::close()
{
	endwin();
	curs_set(1);
}

void Terminal::sleep(int ms)
{
	napms(ms);
}

char Terminal::get_key() const
{
	return wgetch(stdscr);
}
