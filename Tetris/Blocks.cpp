#include "Blocks.h"
#include "Terminal.h"

Blocks::Blocks(int tetrimino) :
    _tetrimino(tetrimino)
{
  switch(_tetrimino) {
    case 1: // T-Tetrimino
      _segments = { {5, 1}, {6, 1}, {4, 1}, {5, 2} };
      break;

    case 2: // S-Tetrimino
      _segments = { {5, 1}, {6, 1}, {5, 2}, {4, 2} };
      break;

    case 3: // Z-Tetrimino
      _segments = { {5, 1}, {4, 1}, {5, 2}, {6, 2} };
      break;

    case 4: // O-Tetrimino
      _segments = { {5, 1}, {6, 1}, {5, 2}, {6, 2} };
      break;

    case 5: // I-Tetrimino
      _segments = { {6, 1}, {7, 1}, {5, 1}, {4, 1} };
      break;

    case 6: // J-Tetrimino
      _segments = { {5, 2}, {6, 2}, {4, 2}, {4, 1} };
      break;

    case 7: // L-Tetrimino
      _segments = { {5, 2}, {4, 2}, {6, 2}, {6, 1} };
      break;

    default: // for debugging, if this appears then smth is fucked
      _segments = { {9, 2}, {8, 2}, {7, 2}, {6, 2}, {5, 2}, {4, 2}, {3, 2} };
      break;
  }
}

void Blocks::draw(Terminal& term)
{
  switch(_tetrimino) {
    case 1: // T-Tetrimino
      for (size_t i = 0; i < _segments.size(); i++) {
          term.set_cell(_segments[i].x, _segments[i].y, 'T', _tetrimino);
      }
      break;

    case 2: // S-Tetrimino
      for (size_t i = 0; i < _segments.size(); i++) {
          term.set_cell(_segments[i].x, _segments[i].y, 'S', _tetrimino);
      }
      break;

    case 3: // Z-Tetrimino
      for (size_t i = 0; i < _segments.size(); i++) {
          term.set_cell(_segments[i].x, _segments[i].y, 'Z', _tetrimino);
      }
      break;

    case 4: // O-Tetrimino
      for (size_t i = 0; i < _segments.size(); i++) {
          term.set_cell(_segments[i].x, _segments[i].y, 'O', _tetrimino);
      }
      break;

    case 5: // I-Tetrimino
      for (size_t i = 0; i < _segments.size(); i++) {
          term.set_cell(_segments[i].x, _segments[i].y, 'I', _tetrimino);
      }
      break;

    case 6: // J-Tetrimino
      for (size_t i = 0; i < _segments.size(); i++) {
          term.set_cell(_segments[i].x, _segments[i].y, 'J', _tetrimino);
      }
      break;

    case 7: // L-Tetrimino
      for (size_t i = 0; i < _segments.size(); i++) {
          term.set_cell(_segments[i].x, _segments[i].y, 'L', _tetrimino);
      }
      break;

    default: // for debugging, if this appears then smth is fucked
      for (size_t i = 0; i < _segments.size(); i++) {
          term.set_cell(_segments[i].x, _segments[i].y, 'A', 8);
      }
  }
}

void Blocks::update(std::vector<Blocks> lockedBlocks) {
  if((!collides_with_bottom()) && (!collides_with_tetriminos(lockedBlocks))) {
    for (size_t i = 0; i < _segments.size(); i++) {
      _segments[i].y += 1;
    }
  }
}

void Blocks::move(char key, std::vector<Blocks> lockedBlocks)
{
    switch(key) {
        case 's':
          if((!collides_with_bottom()) && (!collides_with_tetriminos(lockedBlocks))) {
            for (size_t i = 0; i < _segments.size(); i++) {
              _segments[i].y += 1;
            }
          }
          break;

        case 'd':
          if((!collides_with_right()) && (!collides_with_tetriminos_right(lockedBlocks))) {
            for (size_t i = 0; i < _segments.size(); i++) {
              _segments[i].x += 1;
            }
          }
          break;

        case 'a':
          if((!collides_with_left()) && (!collides_with_tetriminos_left(lockedBlocks))) {
            for (size_t i = 0; i < _segments.size(); i++) {
              _segments[i].x -= 1;
            }
          }
          break;

        default:
            break;
    }
}

void Blocks::rotate(std::vector<Blocks> lockedBlocks) {
  if(_tetrimino != 4) {
    Vec2D pivot = _segments[0]; // rotate around this element
    std::vector<Vec2D> segmentsCopy = _segments;
    std::vector <Vec2D> rotationMatrix = { {0, 1}, {-1, 0} }; // rotate by 90 degrees
    for (size_t i = 1; i < _segments.size(); i++) {
      Vec2D relativeVector = {0, 0};
      Vec2D transformedVector = {0, 0};

      relativeVector.x = (_segments[i].x - pivot.x);
      relativeVector.y = (_segments[i].y - pivot.y);

      transformedVector.x = ((rotationMatrix[0].x * relativeVector.x) + (rotationMatrix[1].x * relativeVector.y));
      transformedVector.y = ((rotationMatrix[0].y * relativeVector.x) + (rotationMatrix[1].y * relativeVector.y));

      _segments[i].x = (pivot.x + transformedVector.x);
      _segments[i].y = (pivot.y + transformedVector.y);
    }
    // check for collision with walls or locked in tetriminos, undo rotation changes if true
    if((collides_with_right()) || (collides_with_left()) || (collides_with_tetriminos(lockedBlocks)) ||
        (collides_with_tetriminos_right(lockedBlocks)) || (collides_with_tetriminos_left(lockedBlocks))) {
      _segments = segmentsCopy;
    }
  }
}

bool Blocks::collides_with_right()
{
  for (size_t i = 0; i < _segments.size(); i++) {
    if (_segments[i].x >= 10) {
      return true;
    }
  }
  return false;
}

bool Blocks::collides_with_left()
{
  for (size_t i = 0; i < _segments.size(); i++) {
    if (_segments[i].x <= 1) {
      return true;
    }
  }
  return false;
}

bool Blocks::collides_with_bottom()
{
  for (size_t i = 0; i < _segments.size(); i++) {
    if (_segments[i].y >= 20) {
      return true;
    }
  }
  return false;
}

bool Blocks::collides_with_tetriminos(std::vector<Blocks> lockedBlocks)
{
  for (size_t i = 0; i < lockedBlocks.size(); i++) { // iterate through all locked blocks
    for (size_t j = 0; j < lockedBlocks[i]._segments.size(); j++) { // iterate through segments of said blocks
      for (size_t k = 0; k < _segments.size(); k++) { // iterate through segments of moving block
        if (((lockedBlocks[i]._segments[j].y) == ((_segments[k].y) + 1)) && (lockedBlocks[i]._segments[j].x == _segments[k].x)) {
          return true;
        }
      }
    }
  }
  return false;
}

bool Blocks::collides_with_tetriminos_left(std::vector<Blocks> lockedBlocks)
{
  for (size_t i = 0; i < lockedBlocks.size(); i++) { // iterate through all locked blocks
    for (size_t j = 0; j < lockedBlocks[i]._segments.size(); j++) { // iterate through segments of said blocks
      for (size_t k = 0; k < _segments.size(); k++) { // iterate through segments of moving block
        if (((lockedBlocks[i]._segments[j].y) == (_segments[k].y)) && (lockedBlocks[i]._segments[j].x == (_segments[k].x - 1))) {
          return true;
        }
      }
    }
  }
  return false;
}

bool Blocks::collides_with_tetriminos_right(std::vector<Blocks> lockedBlocks)
{
  for (size_t i = 0; i < lockedBlocks.size(); i++) { // iterate through all locked blocks
    for (size_t j = 0; j < lockedBlocks[i]._segments.size(); j++) { // iterate through segments of said blocks
      for (size_t k = 0; k < _segments.size(); k++) { // iterate through segments of moving block
        if (((lockedBlocks[i]._segments[j].y) == (_segments[k].y)) && (lockedBlocks[i]._segments[j].x == (_segments[k].x + 1))) {
          return true;
        }
      }
    }
  }
  return false;
}

bool Blocks::collisionCheck(std::vector<Blocks> lockedBlocks)
{
  for (size_t i = 0; i < _segments.size(); i++) {
    _segments[i].y += 1;
  }
  if((collides_with_bottom()) || (collides_with_tetriminos(lockedBlocks))) {
    for (size_t i = 0; i < _segments.size(); i++) {
      _segments[i].y -= 1;
    }
    return true;
  }
  else {
    for (size_t i = 0; i < _segments.size(); i++) {
      _segments[i].y -= 1;
    }
  }
  return false;
}
