#ifndef BLOCKS_H
#define BLOCKS_H

#include <vector>
#include "Vec2D.h"

class Terminal;

class Blocks{
public:

  std::vector<Vec2D> _segments;

  /**
	* Creates new Tetris Block
	*
	* @param tetrimino The tetrimino to be created.
	*/
    Blocks(int tetrimino);

  /**
  * Choses tetrimino to be drawn onto terminal
  *
  * @param term The terminal to draw on
  */
    void draw(Terminal& term);

  /**
  * Update the tetrimino, call within the game loop.
  * Handles the movement of the block.
  * 
  * @param lockedBlocks Vector of blocks that are locked and cannot be moved.
  */
    void update(std::vector<Blocks> lockedBlocks);

  /**
  * Move tetrimino in direction of key press.
  *
  * @param key The key that was pressed.
  * @param lockedBlocks Vector of blocks that are locked and cannot be moved.
  */
    void move(char key, std::vector<Blocks> lockedBlocks);

  /**
   * Rotate tetrimino by 90 degrees.
   */
    void rotate(std::vector<Blocks> lockedBlocks);

  /**
	* Checks if tetrimino collides with right wall
	*
	* @returns True, if a collision occurs, false otherwise
	*/
    bool collides_with_right();

  /**
  * Checks if tetrimino collides with left wall
  *
  * @returns True, if a collision occurs, false otherwise
  */
    bool collides_with_left();

  /**
  * Checks if tetrimino collides with bottom wall
  *
  * @returns True, if a collision occurs, false otherwise
  */
    bool collides_with_bottom();

  /**
  * Checks if tetrimino collides with locked in place tetriminos
  * 
  * @param lockedBlocks Vector of blocks that are locked and cannot be moved.
  * @returns True, if a collision occurs, false otherwise
  */
    bool collides_with_tetriminos(std::vector<Blocks> lockedBlocks);

  /**
  * Checks if tetrimino collides with locked in place tetriminos after left movement
  * 
  * @param lockedBlocks Vector of blocks that are locked and cannot be moved.
  * @returns True, if a collision occurs, false otherwise
  */
    bool collides_with_tetriminos_left(std::vector<Blocks> lockedBlocks);

  /**
  * Checks if tetrimino collides with locked in place tetriminos after right movement
  * 
  * @param lockedBlocks Vector of blocks that are locked and cannot be moved.
  * @returns True, if a collision occurs, false otherwise
  */
    bool collides_with_tetriminos_right(std::vector<Blocks> lockedBlocks);

  /**
  * Checks if tetrimino is going to collide with tetriminos or with
  * the bottom border after next update
  * 
  * @param lockedBlocks Vector of blocks that are locked and cannot be moved.
  * @returns True, if a collision is going to occur, false otherwise
  */
    bool collisionCheck(std::vector<Blocks> lockedBlocks); 

private:
    int _tetrimino;
};

#endif
