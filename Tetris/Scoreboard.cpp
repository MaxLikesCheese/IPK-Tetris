#include "Scoreboard.h"
#include "Terminal.h"

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <iterator>
#include <unistd.h>

Scoreboard::Scoreboard() :
    _scoreboard("scoreboard.txt")
{
     _playerName = "";
    _showScoreBoard = false;
}

void Scoreboard::addToScoreboard(int score)
{
    std::rename(".scoreboard.txt", "scoreboard.txt"); // unhide file to write to it
    std::string scoreString = std::to_string(score);
    std::ofstream txtFile(_scoreboard, std::ios_base::app);
    if(txtFile.is_open()) {
        txtFile << _playerName << "\n";
        txtFile << scoreString << "\n";
        txtFile.close();
    }
}

void Scoreboard::askForName(Terminal& term, int half_width, int half_height) {
    char ch;

    while ((ch=tolower(term.get_key()))!= '\n')
    {
        switch(ch) {
            case 'a': _playerName.push_back(toupper(ch)); break;
            case 'b': _playerName.push_back(toupper(ch)); break;
            case 'c': _playerName.push_back(toupper(ch)); break;
            case 'd': _playerName.push_back(toupper(ch)); break;
            case 'e': _playerName.push_back(toupper(ch)); break;
            case 'f': _playerName.push_back(toupper(ch)); break;
            case 'g': _playerName.push_back(toupper(ch)); break;
            case 'h': _playerName.push_back(toupper(ch)); break;
            case 'i': _playerName.push_back(toupper(ch)); break;
            case 'j': _playerName.push_back(toupper(ch)); break;
            case 'k': _playerName.push_back(toupper(ch)); break;
            case 'l': _playerName.push_back(toupper(ch)); break;
            case 'm': _playerName.push_back(toupper(ch)); break;
            case 'n': _playerName.push_back(toupper(ch)); break;
            case 'o': _playerName.push_back(toupper(ch)); break;
            case 'p': _playerName.push_back(toupper(ch)); break;
            case 'q': _playerName.push_back(toupper(ch)); break;
            case 'r': _playerName.push_back(toupper(ch)); break;
            case 's': _playerName.push_back(toupper(ch)); break;
            case 't': _playerName.push_back(toupper(ch)); break;
            case 'u': _playerName.push_back(toupper(ch)); break;
            case 'v': _playerName.push_back(toupper(ch)); break;
            case 'w': _playerName.push_back(toupper(ch)); break;
            case 'x': _playerName.push_back(toupper(ch)); break;
            case 'y': _playerName.push_back(toupper(ch)); break;
            case 'z': _playerName.push_back(toupper(ch)); break;
            default: break;
        }
        term.draw_text(half_width - _playerName.size()/2, half_height + 1, _playerName);
    }
    _showScoreBoard = true;
}

void Scoreboard::draw(Terminal& term, int score) {
    if(!_showScoreBoard) {
        std::string name_text = "Please enter your name(10 characters max.), press enter once done.";

        int half_width = (term.width() / 2) - 1;
        int half_height = (term.height() / 2) - 1;

        term.draw_text(half_width - name_text.size()/2, half_height, name_text);

        askForName(term, half_width, half_height);
        addToScoreboard(score);
    }
    showScoreboard(term);
}

void Scoreboard::showScoreboard(Terminal& term) {
    term.clear();
    for(size_t i = 0; i <= 20; i++) {
        term.set_cell(0, i, '*', 7);
        term.set_cell(i, 0, '*', 7);
    }

    for(size_t i = 0; i <= 20; i++) {
        term.set_cell(20, i, '*', 7);
        term.set_cell(i, 20, '*', 7);
    }

    std::string currentLine;
    std::vector<std::string> playerScores;
    std::vector<std::string> players;
    std::vector<int> scores;
    std::multimap<int, std::string, std::greater<int>> sortedPlayerScores;

    std::ifstream myfile (_scoreboard);

    if(myfile.is_open()) {
        while(getline(myfile, currentLine)) { // get all lines from file into vector
            playerScores.push_back(currentLine);
        }
        myfile.close();
    }
    for(size_t i = 0; i < playerScores.size(); i++) {
        if(i % 2 == 0) {
            players.push_back(playerScores[i]);
        }
        else {
            int currentScore = std::stoi(playerScores[i]);
            scores.push_back(currentScore);
        }
    }
    
    for(size_t i = 0; i < playerScores.size()/2; i++) {
        sortedPlayerScores.insert(std::make_pair(scores[i], players[i]));
    }

    int counter = 0;
    for(std::multimap<int, std::string>::iterator it = sortedPlayerScores.begin(); it != sortedPlayerScores.end(); it++) {
        if(counter < 10) { // only show top 10
            counter++;
            std::string currentPlayer = it->second;
            currentPlayer.resize(10); // only show first 10 chars of name
            term.draw_text(2, counter, currentPlayer);
            std::string currentScore = std::to_string(it->first);
            term.draw_text(16, counter, currentScore);
        }
    }
}

void Scoreboard::setPlayerName(std::string newPlayer) {
    _playerName = newPlayer;
}

void Scoreboard::setShowScoreBoardBool(bool newBool) {
    _showScoreBoard = newBool;
}

