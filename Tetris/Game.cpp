#include "Game.h"
#include <random>

Game::Game(Terminal& term) :
    _elapsed_time(0.f),
    _score(0),
    _gameSpeed(40),
    _gameSpeedCounter(0),
    _game_over(false),
    _is_done(false),
    _draw_scoreboard(false),
    _term(term),
    _scoreboard(),
    _block(new_tetrimino())
{
    _updateAlreadyBlocked = false;
    _lockedBlocks = {};

    _walls = {
        Wall(Vec2D(0, 0), Vec2D(0, _term.height() - 1)), //left border of wall
        Wall(Vec2D(0, _term.height() - 1), Vec2D(_term.width() - 1, _term.height() - 1)), //bottom border
        Wall(Vec2D(_term.width() - 1, 0), Vec2D(_term.width() - 1, _term.height() - 1)) //right border
    };
}

bool Game::is_done()
{
    return _is_done;
}

void Game::update(float dt)
{
    bool alreadMoved = false; // only move R/L once
    bool alreadyRotated = false; // only rotate once
    char pressed = _term.get_key();

    int fps = 1000/60; // this bad boy runs 60 fps even on potatoes, quality code right there
    _term.sleep(fps);
    _gameSpeedCounter += 1;

    switch (pressed) {
        case 'r':
            _game_over = false;
            _lockedBlocks.clear();
            _block = Blocks(new_tetrimino());
            _score = 0;
            _draw_scoreboard = false;
            _scoreboard.setShowScoreBoardBool(false);
            _scoreboard.setPlayerName("");
            std::rename("scoreboard.txt", ".scoreboard.txt"); // hide file
            break;

        case 'z':
            alreadyRotated = true;
            if(alreadyRotated) {
                _block.rotate(_lockedBlocks);
            }
            break;

        case 'q':
            _is_done = true;
            std::rename("scoreboard.txt", ".scoreboard.txt"); // hide file
            break;

        case 'w': case 'a': case 's': case 'd': //change direction after key press
            alreadMoved = true;
            if(alreadMoved) {
              _block.move(pressed, _lockedBlocks);
            }
            break;

        default:
            break;
    }

    _elapsed_time += dt;
    if (_elapsed_time >= 0.1f)
    {
        if (!_game_over) {
            // check if block is gonna collide after next move, reset counter if true
            if((_block.collisionCheck(_lockedBlocks)) && (!_updateAlreadyBlocked)) {
                    _gameSpeedCounter = 0;
                    _updateAlreadyBlocked = true;
                }
            
            //check if is going to collide and if counter up, to give moving ability for a few more frames
            if (((_block.collides_with_bottom()) || (_block.collides_with_tetriminos(_lockedBlocks))) && (_gameSpeedCounter >= _gameSpeed)) {
                _lockedBlocks.push_back(_block); // add to locked in place vector
                for(size_t i = 0; i < _block._segments.size(); i++) { // check for multiple rows
                    if(row_full()) {
                        _score += 10;
                    }
                }
                _block = Blocks(new_tetrimino()); // generate new tetrimino
            }

            if (_gameSpeedCounter >= _gameSpeed) { // force downwards move once gameSpeed # of frames have passed
                _block.update(_lockedBlocks);
                _updateAlreadyBlocked = false;
                _gameSpeedCounter = 0;
            }

            for(size_t i = 0; i < _lockedBlocks.size(); i++) {
                for (size_t j = 0; j < _lockedBlocks[i]._segments.size(); j++) {
                    if (_lockedBlocks[i]._segments[j].y <= 2) {
                        _game_over = true;
                    }
                }
            }
        }
        _elapsed_time = 0.f;
    }
}

bool Game::row_full()
{
    for (int i = 1; i <= 20; i++) {
        std::vector<Vec2D> currentRow;
        for (size_t j = 0; j < _lockedBlocks.size(); j++) {
            for (size_t k = 0; k < _lockedBlocks[j]._segments.size(); k++) {
                if(_lockedBlocks[j]._segments[k].y == i) { // check if y value same as current row i
                    currentRow.push_back(_lockedBlocks[j]._segments[k]);
                }
            }
        }
        if(currentRow.size() == 10) { // check if a row is full
            for (size_t j = 0; j < _lockedBlocks.size(); j++) {
                for (size_t k = 0; k < _lockedBlocks[j]._segments.size(); k++) {
                    for(size_t n = 0; n < currentRow.size(); n++) {
                        if(_lockedBlocks[j]._segments[k].y == currentRow[n].y) { // search for pieces with y value same as current row
                            _lockedBlocks[j]._segments.erase(_lockedBlocks[j]._segments.begin() + k); // erase pieces out of vector
                            k--; // erase decreases size by one, need to adjust iterator accordingly
                        }
                    }
                }
            }

            for (size_t j = 0; j < _lockedBlocks.size(); j++) {
                for (size_t k = 0; k < _lockedBlocks[j]._segments.size(); k++) {
                    if(_lockedBlocks[j]._segments[k].y <= i) { // check if piece is above deleted row
                        _lockedBlocks[j]._segments[k].y += 1; // move down by one accordingly
                    }
                }
            }
            return true;
        }
    }
    return false;
}

void Game::draw()
{
    if ((_game_over) && (!_draw_scoreboard)) {
        draw_game_over();
        return;
    }

    _term.clear();
    if((_game_over) && (_draw_scoreboard)) {
        _scoreboard.draw(_term, _score);
    }

    if(!_game_over) {
        for (std::size_t i = 0; i < _walls.size(); ++i) {
            _walls[i].draw(_term);
        }
        if(_lockedBlocks.size() > 0) {
            for (size_t i = 0; i < _lockedBlocks.size(); i++) {
            _lockedBlocks[i].draw(_term);
            }
        }
        _block.draw(_term);
    }
}

void Game::draw_game_over()
{
    std::string score_text = "Game Over! You got '" + std::to_string(_score) + "' points.";
    std::string scoreboard_text = "Would you like to add your name and score to the scoreboard? (y/n)";
    std::string reset_text = "Press 'r' to restart, 'q' to exit.";

    int half_width = (_term.width() / 2) - 1;
    int half_height = (_term.height() / 2) - 1;

    _term.draw_text(half_width - score_text.size()/2, half_height, score_text);
    _term.draw_text(half_width - scoreboard_text.size()/2, half_height + 1, scoreboard_text);
    char yesOrNo = _term.get_key();
    switch(yesOrNo) {
        case 'y': 
            _draw_scoreboard = true;
            break;
        case 'r':
            _game_over = false;
            _lockedBlocks.clear();
            _block = Blocks(new_tetrimino());
            _score = 0;
            break;
        case 'q':
            _is_done = true;
            break;
        default:
            break;
    }
    _term.draw_text(half_width - reset_text.size()/2, half_height + + 3, reset_text);
}

int Game::new_tetrimino()
{
    static std::random_device rd;
    static std::mt19937 gen(rd());
    static std::uniform_int_distribution<> dis_tetrimino(1, 7);

    return { dis_tetrimino(gen) };
}
