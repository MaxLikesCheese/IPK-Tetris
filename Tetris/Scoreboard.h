#ifndef SCOREBOARD_H
#define SCOREBOARD_H

#include <string>

class Terminal;

class Scoreboard {
public:
    /**
	* Creates new Scoreboard
	*
	*/
    Scoreboard();

    /**
    * Takes player name input and adds
    * his name and score to scoreboard
    * 
    * @param score Score of the current player
    */
    void addToScoreboard(int score);

    /**
    * Get input of player's name.
    * 
    * @param term Terminal to draw on.
    * @param half_width Half width for x coord. to draw text on.
    * @param half_width Half height for y coord. to draw text on.
    */
    void askForName(Terminal& term, int halt_width, int half_height);

    /**
     * Draws the scoreboard.
     * 
     * @param term Terminal to draw on.
     * @param score Score of current player.
     */
    void draw(Terminal& term, int score);

    /**
     * Shows sorted scoreboard.
     * 
     * @param term Terminal to draw on.
     */
    void showScoreboard(Terminal& term);

    /**
     * Sets _playerName string.
     * 
     * @param newPlayer String it is set to.
     */
    void setPlayerName(std::string newPlayer);

    /**
     * Sets _showScoreBoard boolean.
     * 
     * @param newBool Boolean it is set to.
     */
    void setShowScoreBoardBool(bool newBool);

private:
    std::string _scoreboard;
    std::string _playerName;
    bool _showScoreBoard;
};

#endif