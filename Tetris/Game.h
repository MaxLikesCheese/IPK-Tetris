#ifndef GAME_H
#define GAME_H

#include <vector>
#include "Terminal.h"
#include "Vec2D.h"
#include "Scoreboard.h"
#include "Blocks.h"
#include "Wall.h"


class Game
{
public:
	/**
	* Initializes a new game
	*
	* @param term The terminal to draw our game on
	*/
    Game(Terminal& term);

	/**
	* Checks whether we are done with our game and should
	* end the game loop.
	*
	* @return True if the game loop is done
	*/
    bool is_done();

	/**
	* Advances the game by one step (also called frame)
	*
	* @param dt The time elapsed since the last step in seconds.
  * @param gameSpeed The speed of the game
  * @param gameSpeedCounter Counter for how many frames passed
	*/
    void update(float dt);

	/**
   * Check if a row is full, if true delete row and move
   * every piece above down by one
   * 
   * @param lockedBlocks Vector of blocks that are locked and cannot be moved.
   * @returns True, if a row is full, false otherwise
   */
 	 bool row_full();

	/**
	* Draw the game.
	*/
    void draw();

private:
    void draw_game_over();
    int new_tetrimino();

    float _elapsed_time;
    int _score;
	bool _updateAlreadyBlocked;
	int _gameSpeed;
    int _gameSpeedCounter;
    bool _game_over;
    bool _is_done;
    bool _draw_scoreboard;
    Terminal& _term;
    Scoreboard _scoreboard;
    Blocks _block;
    std::vector<Blocks> _lockedBlocks;
    std::vector<Wall> _walls;
};

#endif
