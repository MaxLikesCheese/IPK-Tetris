#include "Wall.h"
#include "Vec2D.h"
#include "Terminal.h"

Wall::Wall(const Vec2D& start, const Vec2D& end) :
    _start(start), _end(end)
{
}

void Wall::draw(Terminal& term)
{
    if (is_vertical_wall()) {
        for (int y = _start.y; y <= _end.y; ++y) {
            term.set_cell(_start.x, y, 'x', 9);
        }
    }
    else if (is_horizontal_wall())
    {
        for (int x = _start.x; x <= _end.x; ++x) {
            term.set_cell(x, _start.y, 'x', 9);
        }
    }
}

bool Wall::is_vertical_wall() const
{
    if (_start.x == _end.x && _start.y != _end.y) {
        return true;
    }

    return false;
}

bool Wall::is_horizontal_wall() const
{
    if (_start.y == _end.y && _start.x != _end.x) {
        return true;
    }

    return false;
}
