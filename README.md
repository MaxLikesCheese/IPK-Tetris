# ASCII-Tetris in C++

My try at making a basic version of Tetris in C++.

If your Terminal does not support color the output will either look ugly or not work. If this is the case try running within an IDE's Terminal.

Compile with `$ code` in the main dir and then run executable with `$ ./tetris`.
Or compile in **/Tetris** Folder with `g++ Terminal.cpp Game.cpp Wall.cpp Blocks.cpp Scoreboard.cpp main.cpp -g -Wall -std=c++11 -lncurses -o a.out` and then run the executable with `./a.out`.

The latter sometimes creates pre-compiled headers. Make sure to delete those before recompiling if you make any changes to the code as they can give you weird errors. See below for file example.
> Terminal.h.gch
