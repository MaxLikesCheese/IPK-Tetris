TARGET = tetris
LIBS = -lncurses
CC = g++
CFLAGS = -g -Wall -std=c++11

.PHONY: default all clean run $(TARGET)

default: $(TARGET)
all: default

SOURCES = Tetris/Terminal.cpp Tetris/Game.cpp Tetris/Wall.cpp Tetris/Blocks.cpp Tetris/Scoreboard.cpp Tetris/main.cpp
HEADERS = Tetris/Terminal.h Tetris/Game.h Tetris/Wall.h Tetris/Blocks.h Tetris/Scoreboard.h Tetris/Vec2D.h

$(TARGET):
	$(CC) $(SOURCES) $(CFLAGS) $(LIBS) -o $@

run:
	./$(TARGET)

clean:
	-rm -f $(TARGET)
